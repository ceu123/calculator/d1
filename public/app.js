function addNum()
{
    var n1=Number(document.calc.num1.value);
    var n2=Number(document.calc.num2.value);
    var res = n1 + n2;

    document.calc.result.value = res;

}

function subNum(){
    var n1, n2, res;
    n1=Number(document.calc.num1.value);
    n2=Number(document.calc.num2.value);
    res = n1 - n2;

    document.calc.result.value = res;
    
}

function multiplyNum(){
    var n1, n2, res;
    n1=Number(document.calc.num1.value);
    n2=Number(document.calc.num2.value);
    res = n1 * n2;

    document.calc.result.value = res;
    
}

function divideNum() {
    var n1, n2, res;
    n1=Number(document.calc.num1.value);
    n2=Number(document.calc.num2.value);
    res = n1 / n2;

    document.calc.result.value = res;
    
}

function moduloNum() {
    var n1, n2, res;
    n1=Number(document.calc.num1.value);
    n2=Number(document.calc.num2.value);
    res = n1 % n2;

    document.calc.result.value = res;
    
}

